# eln

A command-line tool for quick math computations

![PyPI - License](https://img.shields.io/pypi/l/eln?style=flat-square)
![PyPI - Python Version](https://img.shields.io/pypi/pyversions/eln?style=flat-square)
![PyPI](https://img.shields.io/pypi/v/eln?style=flat-square)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/lehvitus/eln/development?style=flat-square)
![PyPI - Downloads](https://img.shields.io/pypi/dm/eln?style=flat-square)

- [eln](#eln)
    - [Requirements](#requirements)
    - [Installation](#installation)
    - [Commands](#commands)
        - [Azuracast](#azuracast)
        - [DigitalOcean](#digitalocean)
        - [News API](#news-api)
        - [Stub](#stub)
    - [To Do](#issues)
    - [Pull requests](#pull-requests)
    - [License](#license)

## Requirements
[Check requirements](Pipfile).

## Installation
```bash
pip install eln
```

## Commands
### Azuracast
Manage your Azuracast installation.

### DigitalOcean
DigitalOcean API Wrapper.

### News API
News powered by [NewsAPI.org](https://newsapi.org)

### Stub
Stub out sample projects.

## Issues
[Check out issues](https://github.com/lehvitus/eln/issues)

## Pull Requests
Found a bug? See a typo? Have an idea for new command? 
Feel free to submit a [pull request](https://github.com/lehvitus/eln/issues)
with your contributions. They are much welcome.


## License
**eln** is [BSD Licensed](LICENSE.txt).